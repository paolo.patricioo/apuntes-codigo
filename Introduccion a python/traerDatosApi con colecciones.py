import requests
_ENDPOINT="https://pro-api.coinmarketcap.com"
def _url(api):
        return _ENDPOINT+api

def esMoneda(cripto):
        return cripto in monedas

headers = {  
        'Accepts': 'application/json',  
        'X-CMC_PRO_API_KEY':  'fb5aae39-3305-4c7e-adbf-6a8c8e017ed0'
        }
monedas = () 
infoCriptos = {}

data=requests.get(_url("/v1/cryptocurrency/quotes/latest?&symbol=BTC"),headers=headers).json()
for id in data["data"]:
        infoCriptos[data["data"][id]["symbol"]] = data["data"][id]["quote"]["USD"]["price"]

monedas=infoCriptos.keys()

moneda=input("Ingrese nemotecnico a buscar")
while not esMoneda(moneda):
        print("moneda invalida")
        moneda=input("Ingrese nemotecnico a buscar")
else:
        print(moneda," es valida por coinmarketcap.com y su valor es:",infoCriptos.get(moneda))
