import requests
_ENDPOINT="https://api.binance.com"

def _url(api):
        return _ENDPOINT+api
        
def get_price(moneda):
        return requests.get(_url("/api/v3/ticker/price?symbol="+moneda))

def validaMoneda(moneda):
        criptos = ["BTC", "BCC", "LTC", "ETH", "ETC" , "XRP"]
        return moneda in criptos

def esNumero(numero):
        return numero.replace('.','',1).isdigit()

i=0
criptos = []
criptoWallet = []
criptoValor = []
valorTotal = 0

while i<3:
        moneda = input("Ingrese el nombre de la moneda: ")
        while not validaMoneda(moneda):
                print("moneda invalida")
                moneda=input("ingrese el nombre de la moneda: ")
        else:
                criptos.append(moneda)
                data = get_price(moneda+"USDT").json()
                criptoValor.append(float(data["price"]))
                cantidad = input("Ingrese la cantidad de monedas: ")
                while not esNumero(cantidad):
                        print("el valor ingresado no es un numero real")
                        cantidad = float(input("Ingrese la cantidad de monedas: "))
                else:
                        criptoWallet.append(cantidad)
        i+=1

i=0
while i<3:
        valorTotal=(float(criptoWallet[i])*float(criptoValor[i]))
        print("Moneda",criptos[i],
                ", Cantidad",float(criptoWallet[i]),
                ", Valor",float(criptoValor[i]),
                ", valor wallet", float(valorTotal)
                )
        i+=1
