from abc  import ABC, abstractmethod
class Figura(ABC):
	def __init__(self,nombre):
		self.nombre=nombre

	@abstractmethod
	def area(self):
		pass
	def perimetro(self):
		pass
class Rectangulo(Figura):
	def __init__(self,nombre,base,altura):
		super().__init__(nombre)
		self.base=base
		self.altura=altura
	def area(self):
		return self.base*self.altura
	def perimetro(self):
		return 2*(self.base+self.altura)
rect= Rectangulo("rectangulo",1,2)
cuad= Rectangulo("cuadrado",1,1)
print(rect.nombre,rect.area(),rect.perimetro())
print(cuad.nombre,cuad.area(),cuad.perimetro())