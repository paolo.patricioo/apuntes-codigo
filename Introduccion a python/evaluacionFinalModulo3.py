import requests
_ENDPOINT="https://api.binance.com"

def _url(api):
        return _ENDPOINT+api
        
def validaMoneda(moneda):
        criptos = ["BTC", "BCC", "LTC", "ETH", "ETC" , "XRP"]
        if moneda not in criptos:
                return False
        else:
                return True

def get_price(moneda):
        return requests.get(_url("/api/v3/ticker/price?symbol="+moneda))

cripto=""
while not validaMoneda(cripto):
        cripto = input("Ingrese el nombre de la moneda: ")
data = get_price(cripto+"USDT").json()
print("El precio de",cripto,"es",data["price"])

