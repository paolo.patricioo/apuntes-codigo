var texto="";
var resultado = false;
var evento={
	nombre:"Paolo Espinoza",
	descripcion:"Estudio javascript",
	fecha: new Date(2021,10,6)
};
var fechaMinima=new Date(2021,10,1);

function validaString(cadena ,largo) {
  resultado = false;
	if(cadena==null || cadena == undefined){
		texto="verifique el nombre ingresado";
		resultado=false;
	}else{
		if(cadena < largo){
			texto="el nombre debe tener mas de 8 caraceres";
			resultado=false;
		}else{
      console.log("todo bien con le nombre");
			resultado=true;
		}
	}
	return resultado;
}

function validaFecha(fecha , fechaMinima) {
  resultado = false;
	if(fecha==null || fecha == undefined || fechaMinima==null || fechaMinima == undefined){
		resultado=false;
	}else{
		if(fecha < fechaMinima){
			resultado=false;
		}else{
      console.log("todo bien con la fecha");
			resultado=true;
		}
	}
	return resultado;
}

function validaEvento(evento) {
	var res=false;
	if(validaString(evento.nombre.trim(),8)==true){
		if(validaFecha(evento.fecha, fechaMinima)==true){
			res=true;
		}else{
			res=false;
		}
	}else{
		res=false;
	}
	return res;
}

console.log(validaEvento(evento));

