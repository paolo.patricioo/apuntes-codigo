class Evento {
	constructor(){
		this.id=1;
		this.nombre="";
		this.cantidadAsistentes=1;
		this.precioPorAsistente=3;
	}

	enlace(){
		return `/${this.nombre}/${this.id}`;
	}

	dineroTotalRecaudado() {
		var total = this.cantidadAsistentes * this.precioPorAsistente;
		return total;
	}
}

class Actividad extends Evento{
	constructor(id,nombre,cantidadAsistentes,precioPorAsistente){
		super();
		this.id=id;
		this.nombre=nombre;
		this.cantidadAsistentes=cantidadAsistentes;
		this.precioPorAsistente=precioPorAsistente;
	}
}

var eve = new Actividad(1,"evento",10,10000);
console.log(eve.enlace());
console.log(eve.dineroTotalRecaudado());