function maximo(num){
  var _max=0;
	for (i of num){
    if(_max<i){
      _max = i;
    }    
  }
  return _max;
}

function minimo(num){
  var _min=0;
  for (i of num){
    if(_min>i){
      _min = i;
    }    
  }
  return _min;
} 

function promedio(min,max,donaciones){
	//una variable para sumar cada item del arreglo
	var total=0;
	for(i of donaciones){
		total+=i;
	}
	//una variable para saber el total del arreglo
	var totalcant=donaciones.length;
	var promedio = total/totalcant;

	return promedio;
}

var donaciones=[5,15,22,25,30,52,5,1,0];
var _max = maximo(donaciones);
var _min = minimo(donaciones);
console.log(_min);
console.log(_max);
console.log(promedio(_min,_max,donaciones)); 