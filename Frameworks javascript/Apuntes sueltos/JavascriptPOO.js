class Persona{
	constructor(){
		this.nombre="Paolo";
		this.apellido="Espinoza";
	}
	nombreCompleto(){
		return `${this.nombre} ${this.apellido}`
	}
}

var p = new Persona();

class Mascota{
	constructor(nombreMascota){
		this.apodo=nombreMascota;
	}
	setPropietario(propietario){
		this.propietario = propietario;
	}
}

var m1 = new Mascota("Terry");
console.log(m1.apodo);
m1.setPropietario(p)
console.log(m1.propietario.nombreCompleto());