function saludar(amigo){
	console.log(`Hola ${amigo}`);
}

//saludar('ana');

function saludarYMas(amigo, otraFuncion) {
	console.log(`Hola ${amigo}`);
	otraFuncion(amigo);
}

function fin(amigo) {
	console.log(`Fin... ${amigo}`);
}

//saludarYMas('Juan', fin);

var amigos = ['Paolo','Diandra','Edna Moda'];

amigos.forEach(function (amigo, index) {
	console.log(`Hola... ${amigo} , ${index}`);
});