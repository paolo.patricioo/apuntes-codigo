class Persona{
	constructor(){
		this.nombre="Paolo";
		this.apellido="Espinoza";
	}
	nombreCompleto(){
		return `${this.nombre} ${this.apellido}`
	}
}

class Empleado extends Persona{
	constructor(id){
		super();
		this.id=id;
	}
}

var emp = new Empleado(123);
console.log(emp.id);
console.log(emp.nombre);
console.log(emp.apellido);