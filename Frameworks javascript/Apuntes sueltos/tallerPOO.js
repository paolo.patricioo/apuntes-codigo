class ElementoInstitucional{
	constructor(){
		this.id="";
		this.tipo ="";
		this.version ="";
	}

	creaArchivo(){
		return `COMP18S_${this.id}_${this.tipo}_${this.version}.midoc`;
	}
}

class Actividad extends ElementoInstitucional{
	constructor(id,tipo,version){
		super();
		this.id=id;
		this.tipo=tipo;
		this.version=version;
	}
}

var act = new Actividad(1,"interactiva","V1");
console.log(act.creaArchivo());
act = new Actividad(2,"ejercicio","V2");
console.log(act.creaArchivo()); 
act = new Actividad(3,"laboratorio","V3");
console.log(act.creaArchivo()); 