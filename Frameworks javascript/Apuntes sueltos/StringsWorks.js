var que="";
var donde="";
var nombre="";
var apellido="";

function encabezadoEvento(que,donde){
  var texto="Lugar: "+que.trim()+", donde: "+donde.trim();
  return texto;
}

function limpiarNombreParticipante(nombre,apellido){
  var texto=apellido.trim().toUpperCase()+", "+nombre.trim().toLowerCase();
  return texto;
}

console.log(encabezadoEvento("Intercambio de Libros", "Biblioteca"));
console.log(limpiarNombreParticipante("Paolo", "   Espinoza"));