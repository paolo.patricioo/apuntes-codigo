//Define las variables que necesites
var urlJSON = "http://127.0.0.1:5500/Frameworks%20javascript/Unidad%204%20-%20Javascript%20avanzado/Leccion%203/Leccion%204%20-%20Ejercicio%202/info.json";
var fecha;
var html;

$(document).ready(function () {

    //Carga los datos que estan en el JSON (info.json) usando AJAX
    $.ajax({
        url: urlJSON
    }).done(function (response) {
        var eventos = response.eventos;
        fecha = response.fechaActual;
        var pasados = eventos.filter(function (elem) {
            return elem.fecha < fecha;
        })
        eventoPasados = pasados.sort((a,b) => {
            if(a.fecha > b.fecha){
                return -1;
            }
            if(a.fecha < b.fecha){
                return 1;
            }
		return 0;
	    });
        
        for (let i = 0; i < eventoPasados.length; i++) {
            html = html + `   
                <p>nombre: ${eventoPasados[i].nombre}</p>
                <p>descripción: ${eventoPasados[i].descripcion}</p>
                <p>invitados: ${eventoPasados[i].invitados}</p>
                <p>fecha: ${eventoPasados[i].fecha}</p>
                <p>lugar: ${eventoPasados[i].lugar}</p>
            `;    
            document.getElementById("pasados").innerHTML = html;
        }
    });

    //Guarda el resultado en variables
  
    //Selecciona los eventos que sean anteriores a la fecha actual del JSON
  
    //Ordena los eventos segun la fecha (los mas recientes primero)
  
    //Crea un string que contenga el HTML que describe el detalle del evento
  
    //Recorre el arreglo y concatena el HTML para cada evento
  
    //Modifica el DOM agregando el html generado
  
  });
  