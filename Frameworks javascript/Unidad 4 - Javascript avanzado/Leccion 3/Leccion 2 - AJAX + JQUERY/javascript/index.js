function cargarDatos() {
    $.ajax({
        url: "http://127.0.0.1:5500/datos.json"
    }).done(function (response) {
       console.log('Resultado: ',response); 
       $("#contenido").html("Hola " + response.nombre);
       $("#contenido").slideDown("slow");
    });
}

$(document).ready(function () {
    $("button.cargar").click(function (e) { 
        cargarDatos();
        
    });
});