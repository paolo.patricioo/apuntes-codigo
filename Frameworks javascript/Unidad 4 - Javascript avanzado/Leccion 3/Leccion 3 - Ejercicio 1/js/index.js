$(document).ready(function () {
    $.ajax({
        url: 'http://127.0.0.1:5500/Frameworks%20javascript/Unidad%204%20-%20Javascript%20avanzado/Leccion%203/Leccion%203%20-%20Ejercicio%201/info.json'
    }).done(function (response) {
        var id = location.search.match(/id=(\d)*/)[1];
        var resultado = response.eventos;
        var evento = resultado.find(function (elem) {
            return elem.id == id;
        })
        var html = `
            <h2>${evento.nombre}</h2>
            <p>${evento.descripcion}</p>
            <p>Fecha: ${evento.fecha}</p>
            <p>Invitados: ${evento.invitados}</p>
            <p>Costo: ${evento.precio}</p>
        `;
        document.getElementById("contenido").innerHTML = html;
    });
});
