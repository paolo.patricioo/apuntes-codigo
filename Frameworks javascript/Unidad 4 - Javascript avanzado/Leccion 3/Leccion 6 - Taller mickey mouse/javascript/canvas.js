var canvas = document.getElementById('lienzo');
var contexto = canvas.getContext('2d');

var centro = {
    x: canvas.width / 2,
    y: canvas.height / 2, 
}

function dibujar() {

    contexto.clearRect(0,0,canvas.width,canvas.height);

    contexto.beginPath();
    contexto.arc(centro.x, centro.y, 50 , 0 , 2 * Math.PI, false);
    contexto.fillStyle = 'black';
    contexto.fill();
    contexto.stroke();

    //orejas
    contexto.beginPath();
    contexto.arc(centro.x -45, centro.y -60 , 30 , 0 , 2 * Math.PI, false);
    contexto.arc(centro.x +45, centro.y -60 , 30 , 0 , 2 * Math.PI, false);
    contexto.fillStyle = 'black';   
    contexto.fill();
}

dibujar();