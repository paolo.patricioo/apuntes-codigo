function calcular() {
    var promise = new Promise(function(resolve, reject) {
        setTimeout(function () {
            const number = parseInt(Math.random() * 10);
            if (number % 2 == 0) {
                resolve(number);    
            }
            else {
                reject("Rechazado");
            }
        },
     1000)
    });
    return promise;
}

async function procesar() {
    try{
        var resultado = await calcular();
        console.log("Resultado", resultado);
    }
    catch(error){
        console.log("Resultado", error);
    }
}

procesar();