function arreglarCuarto() {
    var promesa = new Promise(function (resolve,reject){
            //resolve('Cuarto arreglado');
            reject('Cuarto no arreglado');
        })   
    return promesa;
}

async function procesar() {
    try{
        var resultado = await arreglarCuarto();
        console.log("Resultado", resultado);
    }
    catch(error){
        console.log("Resultado", error);
    }
}

procesar();