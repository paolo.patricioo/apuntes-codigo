function colocarSaludo() {
    var saludo;
    var langNav = window.navigator.language;
    var idiomaUrl = window.location.search;
    var saludoEs = "Bienvenidos!";
    var saludoEng = "Welcome!";

    if(langNav=="es-ES"){
        saludo = saludoEs;
        console.log("elegido 1");
    }else{
        saludo = saludoEng;
        console.log("elegido 2");
    }

    if(idiomaUrl.includes("lang=es")){
        saludo = saludoEs;
        console.log("elegido 3");
    }else
    if (idiomaUrl.includes("lang=en")){
        saludo = saludoEng;
        console.log("elegido 4");
    }

    document.getElementById("saludo").innerText=saludo;
}

colocarSaludo();