var spanErr = document.getElementById("errText");
var btnAceptar = document.getElementById("aceptar");
var text = document.getElementById("nombre");

function verif(formulario) {
    var re = /[0-9]/i
    if(formulario.nombre.value.trim()==0){
        spanErr.innerHTML = "*campo obligatorio";
        return false;
    }
    else if(formulario.nombre.value.length  < 3){
        spanErr.innerHTML = "*campo debe contener mas de dos caracteres";
        return false;
    } 
    else if(re.test(formulario.nombre.value)){
        spanErr.innerHTML = "*campo debe contener solo letras";
        return false;
    }
    
}

//btnAceptar.addEventListener("click", verif);
//btnAceptar.onclick = verif;
