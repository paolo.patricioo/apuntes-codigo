function validar(formulario) {
    limpiaCampos();

    if(formulario.nombre.value.trim().length == 0 || formulario.nombre.value.trim().length < 3){
        alert("Nombre obligatorio");
        var elemento = document.getElementById('errNombre');
        elemento.innerHTML="Campo obligatorio";
        formulario.nombre.focus();
        return false;
    }

    if(formulario.passw.value.trim().length == 0 || formulario.passw.value.trim().length < 5){
        alert("Contraseña obligatoria");
        var elemento = document.getElementById('errPassw');
        elemento.innerHTML="Campo obligatorio";
        formulario.nombre.focus();
        return false;
    }

    if(formulario.passw.value.trim() != formulario.confirmacion.value.trim()){
        alert("Contraseña no coincide");
        var elemento = document.getElementById('errConf');
        elemento.innerHTML="Contraseña no coincide";
        formulario.nombre.focus();
        return false;
    }

    var re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
    if(!re.test(formulario.email.value)){
        alert("mail incorrecto");
        var elemento = document.getElementById('errEmail');
        elemento.innerHTML="Campo obligatorio";
        formulario.nombre.focus();
        return false;
    }

    if(formulario.genero.value == ""){
        alert("genero es obligatorio");
        return false;
    }

    if(formulario.pais.value == ""){
        alert("Pais es obligatorio");
        var elemento = document.getElementById('errPais');
        elemento.innerHTML="Campo obligatorio";
        formulario.nombre.focus();
        return false;
    }

    if(!formulario.terminos.checked){
        alert("Acepte terminos");
        var elemento = document.getElementById('errTerminos');
        elemento.innerHTML="Acepte terminos";
        formulario.nombre.focus();
        return false;
    }

    return true;
}

function limpiaCampos() {
    var elemento = document.getElementById('errNombre');
    elemento.innerHTML="";
    var elemento = document.getElementById('errPassw');
    elemento.innerHTML="";
    var elemento = document.getElementById('errConf');
    elemento.innerHTML="";
    var elemento = document.getElementById('errEmail');
    elemento.innerHTML="";
    var elemento = document.getElementById('errPais');
    elemento.innerHTML="";
    var elemento = document.getElementById('errTerminos');
    elemento.innerHTML="";
}