function aceptando(event) {
    alert('acepté');
}

function aceptandoOtraVez(event) {
    alert('acepté de nuevo');
}

var botonAceptar= document.getElementById("boton-aceptar");

botonAceptar.addEventListener("click", aceptando);
botonAceptar.addEventListener("click", aceptandoOtraVez);